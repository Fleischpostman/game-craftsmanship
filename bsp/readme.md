# Setup

## Windows

### Prerequisites

First of all you will need to several dependencies:

- Microsoft Visual Studio 2022
- scoop

To install all required tools run:

```bash
scoop bucket add sirrobbe https://github.com/SirRobbe/scoop-sirrobbe.git
scoop update
scoop install llvm python umbra
```

For the python build script you will also need dotenv support for python:

```bash
pip install python-dotenv
```

**NOTE**

Before you install the python package it can be usefull to create a own virtual environment for the project.
However, since you only need to install one package and dotenv support is pretty common anyway you can also use your main interpreter.

After that we have all our compilers and tools set up we use for our toolchain.
However we need to install a version of raylib too, as we will use it as a backbone of our project.
One easy way to do this on Windows is with vcpkg.

If you don't have vcpkg installed run:

```bash
git clone https://github.com/Microsoft/vcpkg.git
cd vcpkg
bootstrap-vcpkg.bat
vcpkg integrate install
```

After that run the following command to install raylib:

```bash
vcpkg install raylib
```

In the `vcpkg` directory you should now find the subdirectory `installed/x64-windows`.
This directory should include everything you need to work with raylib.

### Building the project

To build the project you would simply run:

```bash
python build.py
```

However, before you run this the first time you will have to set up the path to your raylib installation.
Otherwise the build script won't find the required raylib binaries.
To do so create a copy of the ***.env.template*** file and simply call it ***.env***.
In the ***.env*** file adjust the value of `RAYLIB_DIR` to your directory of raylib.
After that running the build script should create a working build of the project.

### Common Issues

#### clang was not found

Try the oldest trick in the book. Restart your machine.

## Running the project

Starting the project is quite easy.
Just run:

```bash
build/bsp.exe
```

**NOTE**

Some ancient terminals might not interpret the single slash correctly on Windows.
If that is the case for you try `build//bsp.exe` or `build\bsp.exe` instead.