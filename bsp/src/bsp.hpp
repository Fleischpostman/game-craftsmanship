#pragma once

#include <raylib.h>
#include "types.hpp"

namespace Bsp
{
    struct Line
    {
        Vector2 A;
        Vector2 B;
        Vector2 Normal;
        const char* Name;
    };

    static Line MakeLine(Vector2 a, Vector2 b, b32 isNormalToLeft,
                         const char* name);

    static void DrawLine(Line* line);
}
