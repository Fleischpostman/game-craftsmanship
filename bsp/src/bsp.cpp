#include "bsp.hpp"
#include <raymath.h>

namespace Bsp
{
    static Line MakeLine(Vector2 a, Vector2 b, b32 isNormalToLeft,
                         const char* name)
    {
        Line result = { a, b, {}, name };
        auto d = Vector2Subtract(b, a);

        if(isNormalToLeft)
        {
            result.Normal.x = -d.y;
            result.Normal.y = d.x;
        }
        else
        {
            result.Normal.x = d.y;
            result.Normal.y = -d.x;
        }

        result.Normal = Vector2Normalize(result.Normal);

        return result;
    }

    static void DrawLine(Line* line)
    {
        // NOTE(Fabi): Just some constants that will be used during drawing
        constexpr f32 LineThickness = 5.f;
        constexpr f32 NormalThickness = 2.5f;
        constexpr f32 PointRadius = 6.f;
        constexpr f32 NormalLength = 30.f;
        constexpr Color NormalColor = BLUE;
        constexpr Color LineColor = RED;
        constexpr Color CircleColor = GREEN;

        // NOTE(Fabi): To draw the normal we first need to calculate the center
        //             of the line between A and B. We also scale the normal
        //             because a normal of length one would not be visible.
        auto center = Vector2Scale(Vector2Add(line->A, line->B), 0.5f);
        auto normal = Vector2Scale(line->Normal, NormalLength);
        auto endNormal = Vector2Add(center, normal);
        DrawLineEx(center, endNormal, NormalThickness, NormalColor);

        DrawLineEx(line->A, line->B, LineThickness, LineColor);
        
        DrawCircleV(line->A, PointRadius, CircleColor);
        DrawCircleV(line->B, PointRadius, CircleColor);

        if(line->Name != nullptr)
        {

            normal = Vector2Scale(normal, 0.5f);
            DrawText(line->Name, center.x - normal.x,
                     center.y - 1.5f * normal.y,
                     30,
                     WHITE);
        }
    }
}

