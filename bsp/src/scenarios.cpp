#include "scenarios.hpp"

namespace Bsp
{
    static void DrawScenario(Scenario* scenario)
    {
        for(i32 lineIndex = 0; lineIndex < scenario->LineCount; lineIndex++)
        {
            DrawLine(&scenario->Lines[lineIndex]);
        }
    }

    static void MakeScenarioOne(Scenario* scenario)
    {
        scenario->Lines[0] = MakeLine({500, 500}, {725, 275}, false, "A");
        scenario->Lines[1] = MakeLine({50, 300}, {315, 325}, false, "B");
        scenario->Lines[2] = MakeLine({75, 100}, {200, 140}, true, "C");
        scenario->Lines[3] = MakeLine({125, 375}, {175, 475}, true, "D");
        scenario->Lines[4] = MakeLine({550, 550}, {830, 475}, false, "E");
        scenario->Lines[5] = MakeLine({920, 200}, {1020, 325}, true, "F");

        scenario->LineCount = 6;
    }
}