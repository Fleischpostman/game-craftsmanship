#pragma once
#include "bsp.hpp"

namespace Bsp
{
    constexpr i32 MaxLinesPerScenario = 128;

    struct Scenario
    {
        Line Lines[MaxLinesPerScenario];
        i32 LineCount;       
    };

    static void DrawScenario(Scenario* scenario);

    // --------------------------------------------------------------------- //
    // NOTE(Fabi): The following functions are just used to create different
    //             scenarios as part of the demo. They don't contain any
    //             educational content, but I try to write them as clean as
    //             possible. So feel free to browse through some of them anyway.

    /// @brief Basic example with six lines. The left side of the root will be
    ///        fully balanced. The scenario does not contain any lines that 
    ///        will be split by the planes of the bsp.
    /// @param scenario Destination object the scenario will be written into.
    ///                 The prior state of the scenario will be overwritten.
    static void MakeScenarioOne(Scenario* scenario);
}