#pragma once

#include <stdint.h>

typedef int32_t b32;
typedef int32_t i32;

typedef float f32;
typedef double f64;