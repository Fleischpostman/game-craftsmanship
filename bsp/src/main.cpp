#include "raylib.h"
#include "types.hpp"
#include "scenarios.hpp"

i32 main()
{
    Bsp::Scenario scenario;
    Bsp::MakeScenarioOne(&scenario);

    InitWindow(1280, 720, "Binary Space Partition");

    while(!WindowShouldClose())
    {
        if(IsKeyPressed(KEY_F5))
        {
            TakeScreenshot("screenshot.png");
        }

        BeginDrawing();
        ClearBackground(DARKGRAY);
        Bsp::DrawScenario(&scenario);
        EndDrawing();
    }

    CloseWindow();

    return 0;
}