# Binary Space Partitioning (BSP)

## Table of Contents

- [Introduction](##Introduction)
- [History](##History)
- [Theory](###Theory)
### Implementation
### Final Thoughts

## Introduction

For the first post we're actually starting with the letter B instead of A.
The sole reason therefore is that probably the most logic topic for a game related article starting with the letter A would be the A* algorithm.
In my opinion there is no reason in starting such a blog with an article about A* as there are already lots and lots of good resources it, so writing about A* probably would not have that huge of a benefit.
Moreover, there are probably a lot of other interesting topics you could write about for the letter A but I just didn't want to bother thinking about it for know, so this alphabet will start the letter B for the forseeable future.

With this little rumble out of the way let's start with actual topic of this article: Binary space partitioning, in short: BSP.
BSP is an algorith that can be used to recursivly subdivde a given Euclidean space into two subsets.
Doing this seperation recursivly has the nice advante of dividing the space into small sub spaces that can be traversed by traversing a binary tree, which makes it great for optimizing operations on the whole space.
Hence its main usage is in the form of an acceleration structure for other computational heavy tasks like for example rendering and collision detection.
However, you could use it for any for of partitioning of a game world, e.g. for a "LOD" system of AI agent simulations, although there probably would be better fits for such a scenario.
While it was very prominent in the early 2000s today BSP is not that important anymore, as in many scenarios of BSP k-d trees are used today, as the work in any dimension and because of the increases in graphics computing power which made other rendering techniques that no longer require a BSP more viable.
However it is still a nice algorithm to know, as it can help understanding other partitioning algorithms like k-d trees or octrees and you still may find a scenario where you can apply it.
No one knows, maybe you have to roll out some form of custom collision detection in the future.  

In the next chapter I will give a little bit on insight where BSPs originated, because I think knowing the history of something often helps to better put it into context.
If you're not interested in that at all feel free to jump directly into the next chapter where we will dive into all the details about the algorithm.


## History

BSP was developed in 1969 by military research in the context of 3D rendering for pilot simulations.
One of the biggest topics of that time was hidden surface determination due to the hardware limitations back then.
Because rendering a lot of complex graphics at a high refresh rate was not possible with general purpose CPU computing at that time it was very important to know what exactly to draw in order to render the scene with as few draw calls as possible and also to reduce over draw.
Over draw is the process of drawing something in the background that is occluded by an object further in the front.
As it is not visible all the time needed to render it was wasted looking at the final result.
Therefore, programmers tried the keep the overdraw as low as possible.
Despite sorting the scene was also a very important problem of the time.
It was important to sort the geomerty based on the viewers the position, because drawing the objects behind and other object after the one in front would lead to drawing over it which results in servere artifacts as the scene would not be rendered correctly.

Today most of these problems are solved using a Z-Buffer.
This is an additional represnation of the viewport during rendering that stores depth information for everything that is drawn.
If an object would be drawn at an already drawn to position later on the depth value of the new object is compared to the value in the Z-Buffer.
Is the new object in front it will be drawn and the depth buffer will be updated, is it not the draw call will be discarded.
Howerver back than having the additional memory for a Z-Buffer was very costly, so most techniques tried to use another way to solve this problem.

One such solution was the painter's algorithm.
Be aware the painter's algorithm can cause a lot of over draw and is can't handle many more general scene strucutres by design.
So from todays perspective it really is not a good choice for solving this problem, but we will look at it for its simplicity.