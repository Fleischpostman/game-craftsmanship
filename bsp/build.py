import os
import sys
import shutil
from subprocess import run
from dotenv import load_dotenv

load_dotenv()

build_path = "build"
raylib_dir = os.getenv("RAYLIB_DIR")

os.makedirs(build_path, exist_ok=True)
shutil.copyfile(f"{raylib_dir}/bin/raylib.dll", f"{build_path}/raylib.dll")

completed_command = run(f"umbra --sources src", shell=True, capture_output=True, text=True)

completed_command = run(f"clang build.cpp -o {build_path}/bsp.exe -Wall -I{raylib_dir}/include -l{raylib_dir}/lib/raylib.lib",
                        shell=True,
                        capture_output=True,
                        text=True)

print(completed_command.stdout)
print(completed_command.stderr)
print(f"build returned: {completed_command.returncode}")

sys.exit(completed_command.returncode)